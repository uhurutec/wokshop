import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";

@Component({
  selector: 'nav-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  title = 'nav';

  items: MenuItem[];

  constructor() {
    this.items = [];
  }

  ngOnInit() {

    this.initMenuModel();

  }

  initMenuModel() {

    this.items = [
      {
        label: 'Home',
        icon: 'pi pi-home',
        routerLink: '',
      },
      {
        label: 'Menu',
        routerLink: 'food'
      },
      {
        label: 'Drinks Menu',
        routerLink: '',
      },
      {
        label: 'Events',
        icon: 'pi pi-calendar',
        routerLink: '',
      }

    ];
  }

}
