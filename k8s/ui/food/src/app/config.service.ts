import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private config: any;
  private configLoaded = false;

  constructor(private httpClient: HttpClient) {
  }

  loadConfig() {
    return this.httpClient.get('/config/config.json')
      .toPromise()
      .then( data => {
        this.config = data;
        this.configLoaded = true;
      });
  }

  public getConfig() {
    return this.config;
  }

}
