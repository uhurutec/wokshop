import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Food} from "./food";
import {ConfigService} from "./config.service";

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private httpClient: HttpClient, private configService: ConfigService) { }

  public getFood(type: string): Promise<Food[]> {
    return this.httpClient.get<Food[]>(this.configService.getConfig().foodServiceUrl + '/food/' + type, ).toPromise();
  }
}
