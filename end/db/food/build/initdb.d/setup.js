var Graph = require('@arangodb/general-graph');

db._createDatabase("wokshop-food-db", {}, [{ username: "cloudland", passwd: "wokshop", active: true}]);

db._useDatabase("wokshop-food-db");

// create collections
//
db._create("appetizer");
db._create("maincourse");
db._create("dessert");

// add appetizer
//
db.appetizer.save([{
        "id": "1000",
        "name": "Elitr Sed",
        "description": "Product Description",
        "image": "IMG_20160901_182042.jpg",
        "price": 8.5,
        "category": "Meat"
    },
    {
        "id": "1001",
        "name": "Diam Nonumy Eirmod",
        "description": "Product Description",
        "image": "IMG_20161227_120349.jpg",
        "price": 7,
        "category": "Meat"
    },
    {
        "id": "1002",
        "name": "Tempor Invidunt",
        "description": "Product Description",
        "image": "IMG_20191126_124648.jpg",
        "price": 10,
        "category": "Meat"
    },
    {
        "id": "1003",
        "name": "Ut Labore Et Dolore",
        "description": "Product Description",
        "image": "P1010008.JPG",
        "price": 12.5,
        "category": "Vegetable"
    },
    {
        "id": "1004",
        "name": "Magna Aliquyam",
        "description": "Product Description",
        "image": "P3010106.JPG",
        "price": 7,
        "category": "Vegetable"
    },
    {
        "id": "1005",
        "name": "Erat Sed Diam Voluptua",
        "description": "Product Description",
        "image": "P7280533.JPG",
        "price": 10.5,
        "category": "Seafood"
    },
    {
        "id": "1006",
        "name": "At Vero",
        "description": "Product Description",
        "image": "P8060211.JPG",
        "price": 6,
        "category": "Vegetable"
    },
    {
        "id": "1007",
        "name": "Eos Et Accusam Et",
        "description": "Product Description",
        "image": "P8310372.JPG",
        "price": 8,
        "category": "Seafood"
    },
    {
        "id": "1008",
        "name": "Lorem Ipsum",
        "description": "Product Description",
        "image": "P9100727.JPG",
        "price": 8.5,
        "category": "Meat"
    },
    {
        "id": "1009",
        "name": "Dolor Sit",
        "description": "Product Description",
        "image": "PB100669.JPG",
        "price": 9,
        "category": "Seafood"
    },
    {
        "id": "1010",
        "name": "Amet Consetetur Sadipscing",
        "description": "Product Description",
        "image": "PB240964.JPG",
        "price": 5,
        "category": "Seafood"
    }]);

// add maincourse
//
db.maincourse.save([{
        "id": "1000",
        "name": "Lorem Ipsum Dolor",
        "description": "Product Description",
        "image": "IMG_20160131_132352.jpg",
        "price": 15.5,
        "category": "Meat"
    },
    {
        "id": "1001",
        "name": "Consetetur Sadipscing Elitr",
        "description": "Product Description",
        "image": "IMG_20160420_195724.jpg",
        "price": 16,
        "category": "Meat"
    },
    {
        "id": "1002",
        "name": "Sed Diam Nonumy",
        "description": "Product Description",
        "image": "IMG_20160715_124656.jpg",
        "price": 14.5,
        "category": "Seafood"
    },
    {
        "id": "1003",
        "name": "Eirmod Tempor Invidunt",
        "description": "Product Description",
        "image": "IMG_20160901_182315.jpg",
        "price": 15,
        "category": "Seafood"
    },
    {
        "id": "1004",
        "name": "Ut Labore Et",
        "description": "Product Description",
        "image": "IMG_20161228_085252.jpg",
        "price": 16,
        "category": "Meat"
    },
    {
        "id": "1005",
        "name": "Dolore Magna Aliquyam Erat",
        "description": "Product Description",
        "image": "IMG_20161228_085257.jpg",
        "price": 12.5,
        "category": "Meat"
    },
    {
        "id": "1006",
        "name": "Sed Diam Voluptua",
        "description": "Product Description",
        "image": "IMG_20170107_120714.jpg",
        "price": 18,
        "category": "Meat"
    },
    {
        "id": "1007",
        "name": "At Vero Eos",
        "description": "Product Description",
        "image": "IMG_20170112_105915.jpg",
        "price": 17,
        "category": "Meat"
    },
    {
        "id": "1008",
        "name": "Et Accusam Et Justo",
        "description": "Product Description",
        "image": "IMG_20170503_125536.jpg",
        "price": 17,
        "category": "Vegetable"
    },
    {
        "id": "1009",
        "name": "Duo Dolores Et",
        "description": "Product Description",
        "image": "IMG_20180320_133328.jpg",
        "price": 18.5,
        "category": "Vegetable"
    },
    {
        "id": "1010",
        "name": "Ea Rebum",
        "description": "Product Description",
        "image": "IMG_20180728_202202.jpg",
        "price": 16,
        "category": "Meat"
    },
    {
        "id": "1011",
        "name": "Stet Clita Kasd Gubergren",
        "description": "Product Description",
        "image": "IMG_20180908_183139.jpg",
        "price": 20,
        "category": "Vegetable"
    },
    {
        "id": "1012",
        "name": "No Sea Takimata",
        "description": "Product Description",
        "image": "IMG_20190702_203659.jpg",
        "price": 16.5,
        "category": "Seafood"
    },
    {
        "id": "1013",
        "name": "Sanctus Est Lorem",
        "description": "Product Description",
        "image": "IMG_20191008_185527.jpg",
        "price": 15.5,
        "category": "Seafood"
    },
    {
        "id": "1014",
        "name": "Ipsum Dolor Sit Amet",
        "description": "Product Description",
        "image": "IMG_20191115_065103.jpg",
        "price": 19,
        "category": "Vegetable"
    },
    {
        "id": "1015",
        "name": "Lorem",
        "description": "Product Description",
        "image": "IMG_20191120_151916.jpg",
        "price": 18,
        "category": "Seafood"
    },
    {
        "id": "1016",
        "name": "Ipsum Dolor Sit",
        "description": "Product Description",
        "image": "IMG_20191125_130946.jpg",
        "price": 19,
        "category": "Seafood"
    },
    {
        "id": "1017",
        "name": "Amet Consetetur Sadipscing",
        "description": "Product Description",
        "image": "IMG_20191125_130952.jpg",
        "price": 20.5,
        "category": "Seafood"
    },
    {
        "id": "1018",
        "name": "Elitr Sed Diam",
        "description": "Product Description",
        "image": "IMG_20191126_122720.jpg",
        "price": 17.5,
        "category": "Meat"
    },
    {
        "id": "1019",
        "name": "Nonumy Eirmod",
        "description": "Product Description",
        "image": "IMG_20200226_191621.jpg",
        "price": 21,
        "category": "Seafood"
    },
    {
        "id": "1020",
        "name": "Tempor Invidunt Ut",
        "description": "Product Description",
        "image": "IMG_20201231_183755.jpg",
        "price": 14.5,
        "category": "Meat"
    },
    {
        "id": "1021",
        "name": "Labore Et Dolore Magna",
        "description": "Product Description",
        "image": "P1010001.JPG",
        "price": 14,
        "category": "Seafood"
    },
    {
        "id": "1022",
        "name": "Aliquyam Erat",
        "description": "Product Description",
        "image": "P1010002.JPG",
        "price": 16,
        "category": "Meat"
    },
    {
        "id": "1023",
        "name": "Sed Diam Voluptua At",
        "description": "Product Description",
        "image": "P1010003.JPG",
        "price": 15,
        "category": "Seafood"
    },
    {
        "id": "1024",
        "name": "Vero Eos Et",
        "description": "Product Description",
        "image": "P1010021.JPG",
        "price": 14,
        "category": "Seafood"
    },
    {
        "id": "1025",
        "name": "Accusam",
        "description": "Product Description",
        "image": "P1010022.JPG",
        "price": 16,
        "category": "Seafood"
    },
    {
        "id": "1026",
        "name": "Et Justo Duo Dolores",
        "description": "Product Description",
        "image": "P1010024.JPG",
        "price": 18,
        "category": "Seafood"
    },
    {
        "id": "1027",
        "name": "Et Ea Rebum Stet",
        "description": "Product Description",
        "image": "P1010031.JPG",
        "price": 21.5,
        "category": "Vegetable"
    },
    {
        "id": "1028",
        "name": "Clita Kasd",
        "description": "Product Description",
        "image": "P2200045.JPG",
        "price": 25.5,
        "category": "Seafood"
    },
    {
        "id": "1029",
        "name": "Gubergren",
        "description": "Product Description",
        "image": "P1043112.JPG",
        "price": 15,
        "category": "Seafood"
    }]);

// add dessert
//
db.dessert.save([{
        "id": "1000",
        "name": "Nonumy Eirmod",
        "description": "Product Description",
        "image": "P1010136.JPG",
        "price": 8,
        "category": "Cold"
    },
    {
        "id": "1001",
        "name": "Elitr Sed Diam",
        "description": "Product Description",
        "image": "P2200021.JPG",
        "price": 9,
        "category": "Cold"
    },
    {
        "id": "1002",
        "name": "Consetetur Sadipscing",
        "description": "Product Description",
        "image": "P3314911.JPG",
        "price": 8.5,
        "category": "Cold"
    },
    {
        "id": "1003",
        "name": "Dolor Sit Amet",
        "description": "Product Description",
        "image": "P8010134.JPG",
        "price": 10,
        "category": "Hot"
    },
    {
        "id": "1004",
        "name": "Lorem Ipsum",
        "description": "Product Description",
        "image": "P8070150.JPG",
        "price": 7.5,
        "category": "Hot"
    }]);


