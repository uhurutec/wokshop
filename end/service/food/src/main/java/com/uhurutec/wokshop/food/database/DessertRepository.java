package com.uhurutec.wokshop.food.database;

import com.arangodb.springframework.repository.ArangoRepository;
import com.uhurutec.wokshop.food.database.model.Dessert;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

@Repository
@Service
public interface DessertRepository extends ArangoRepository<Dessert, String> {}
