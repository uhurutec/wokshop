package com.uhurutec.wokshop.food.controller;

import com.uhurutec.wokshop.food.database.AppetizerRepository;
import com.uhurutec.wokshop.food.database.DessertRepository;
import com.uhurutec.wokshop.food.database.MaincourseRepository;
import com.uhurutec.wokshop.food.database.model.Appetizer;
import com.uhurutec.wokshop.food.database.model.Dessert;
import com.uhurutec.wokshop.food.database.model.Maincourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("food")
public class FoodController {
    private final AppetizerRepository appetizerRepository;
    private final MaincourseRepository maincourseRepository;
    private final DessertRepository dessertRepository;

    @Autowired
    public FoodController(
            AppetizerRepository appetizerRepository,
            MaincourseRepository maincourseRepository,
            DessertRepository dessertRepository) {
        this.appetizerRepository = appetizerRepository;
        this.maincourseRepository = maincourseRepository;
        this.dessertRepository = dessertRepository;
    }

    @GetMapping(path = "/{type}")
    public ResponseEntity getFood(@PathVariable("type") String type) {

        switch (type) {
            case "appetizer":
                Iterable<Appetizer> appetizers = this.appetizerRepository.findAll();
                return new ResponseEntity<>(appetizers, HttpStatus.OK);
            case "maincourse":
                Iterable<Maincourse> maincourses = this.maincourseRepository.findAll();
                return new ResponseEntity<>(maincourses, HttpStatus.OK);
            case "dessert":
                Iterable<Dessert> desserts = this.dessertRepository.findAll();
                return new ResponseEntity<>(desserts, HttpStatus.OK);
            default:
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
