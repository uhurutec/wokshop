package com.uhurutec.wokshop.food.config;

import com.arangodb.ArangoDB;
import com.arangodb.springframework.config.ArangoConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArangoDbConfig implements ArangoConfiguration {

    @Autowired private Config config;

    public ArangoDB.Builder arango() {
        return new ArangoDB.Builder()
                .host(config.getDatabaseConfig().getServer(), config.getDatabaseConfig().getPort())
                .user(config.getDatabaseConfig().getUsername())
                .password(config.getDatabaseConfig().getPassword());
    }

    public String database() {
        return "wokshop-food-db";
    }
}
