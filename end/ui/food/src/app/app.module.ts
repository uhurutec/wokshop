import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TabViewModule} from "primeng/tabview";
import { AssetUrlPipe } from './asset-url.pipe';
import { FoodPanelComponent } from './food-panel/food-panel.component';
import {HttpClientModule} from "@angular/common/http";
import {ConfigService} from "./config.service";
import {DataViewModule} from "primeng/dataview";
import {DropdownModule} from "primeng/dropdown";
import {ButtonModule} from "primeng/button";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule} from "@angular/forms";

const appInitializerFn = (configService: ConfigService) => {
  return () => {
    return configService.loadConfig();
  }
};

@NgModule({
  declarations: [
    AppComponent,
    AssetUrlPipe,
    FoodPanelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TabViewModule,
    HttpClientModule,
    DataViewModule,
    DropdownModule,
    ButtonModule,
    BrowserAnimationsModule,
    FormsModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [ConfigService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
