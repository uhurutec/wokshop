import { Component, Input, OnInit } from '@angular/core';
import {Food} from "../food";
import {SelectItem} from "primeng/api";
import {FoodService} from "../food.service";

@Component({
  selector: 'app-food-panel',
  templateUrl: './food-panel.component.html',
  styleUrls: ['./food-panel.component.css']
})
export class FoodPanelComponent implements OnInit {

  @Input() className: string = 'maincourse';

  title = 'food';

  foodList: Food[] = [];

  sortOptions: SelectItem[] = [];
  sortKey: string = '';
  sortOrder: number = 0;
  sortField: string = 'price';

  constructor(private foodService: FoodService) { }

  ngOnInit() {
    this.foodService.getFood(this.className).then(data => this.foodList = data);

    this.sortOptions = [
      {label: 'Price High to Low', value: '!price'},
      {label: 'Price Low to High', value: 'price'}
    ];
  }

  onSortChange(event: { value: any; }) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }
}
