# User DB Container 

Dieser Containter stellt die Datenbank für das User-Management bereit.

Hier werden User und Rollen für die Uview-UI vorgehalten.

Es gibt dazu drei Collections:

- appetizer
- maincourse
- dessert

Die Datenbank wird hier mit Datensätzen initialisiert:

- admin (Rolle: ADMIN, Passwort: admin, Gruppe: group_a)
- guest (Rolle: GUEST, Passwort: guest, Gruppen: group_b, group_c)

Diese Zuordnungen dienen zur Zeit nur der Veranschaulichung und werden in Zukunft gewiss verändert werden.

## Docker installieren

Die Installation von Docker ist im [Wiki](https://wiki.prod.if-lan.de/Entwicklungswerkzeuge/Docker.md) beschrieben.

## Image bauen

```
./wokshop-food-db.sh build
```

## Starten

Container starten mit automatischem Datenbank-Setup im Container.

```
./wokshop-food-db.sh start
```

Der Docker-Container bindet sich per Default an 10.42.1.1:8529 und kann aus jeder Vagrant-Box über das Host-Only-Netzwerk verwendet werden.
Wenn man den Container an ein anderes Socket binden möchte, kann man dies mit der Environment Variable `LISTEN_ADDRESS` steuern:

```sh
LISTEN_ADDRESS=10.42.10.1:68529 ./wokshop-food-db.sh start
```

Danach läuft dieser bis man ihn stoppt (oder der Shutdown des Systems dies tut).

## Stoppen

```
./wokshop-food-db.sh stop
```

## Container Volumes

Der Docker-Container legt seine Datenbank-Daten in einem Docker-Volume namens "wokshop-food-db_arangodb-data" ab.
Diese Daten sind konsistent über den Restart des Containers hinweg.

Um das Volume und den Container zu entfernen, kann man `./wokshop-food-db.sh clean` aufrufen.

## Backup

Starten eines Backup, welches die `dump.json` in das Verzeichnis "dump/" des Repository legt:

```sh
./wokshop-food-db.sh backup
```

Backup-Dump mit komprimierten _Collection Daten_:

```sh
./wokshop-food-db.sh backup --compress-output true
```

## Restore

Den im vorigen erzeugten Backup Dump wiederherstellen:

```sh
./wokshop-food-db.sh restore
```

## Shell starten

Eine `arangosh` starten:

```sh
./wokshop-food-db.sh dbsh
```

Eine `bash` starten:

```sh
./wokshop-food-db.sh bash
```

## FAQ

### Mit welchen Zugangsdaten kann man sich mit der Datenbank `wokshop-food-db` verbinden?

```
Endpoint: http://10.42.1.1:8529
Username: uview
Passwort: uv13w
```

**Falls beim Starten die LISTEN_ADDRESS gesetzt wird, muss der Endpoint entsprechend anders gesetzt werden.**

### Warum kann ich nicht einfach mit docker-compose arbeiten?

Alle Aufrufe des wokshop-food-db.sh Skripts können auch mit docker-compose durchgeführt werden. Das Shell-Skript ist nur eine Vereinfachung zur Arbeit mit dem Container.

### Kann der Container nicht einfach beim Systemstart starten?

Um den Container immer gleich bei Systemstart zu starten, kann man in die Datei `wokshop-food-db.yaml` den Parameter `restart` einfügen. Im konkreten Fall `restart: always`.

Längerer Beispiel-Ausschnitt:
    
```
services:
  wokshop-food-db:
    restart: always
```
