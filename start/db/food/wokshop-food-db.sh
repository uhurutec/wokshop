#!/bin/bash

ACTION=$1
if [ -n "$ACTION" ]; then
    shift;
fi

NAME=wokshop-food-db

APP_DIR=$(dirname "$0")
LISTEN_ADDRESS=${LISTEN_ADDRESS:-8529}
DB_ROOT_PASSWORD=${DB_ROOT_PASSWORD:-wokshop}

export APP_DIR LISTEN_ADDRESS DB_ROOT_PASSWORD

COMPOSE="docker-compose -p $NAME -f $NAME.yaml"
case $ACTION in
    clean)
        $COMPOSE down -v
        ;;
    stop)
        $COMPOSE stop "$@"
        ;;
    down)
        $COMPOSE down "$@"
        ;;
    build)
        $COMPOSE build --pull "$@"
        ;;
    ps)
        $COMPOSE ps "$@"
        ;;
    log|logs)
        $COMPOSE logs "$@"
        ;;
    start|up)
        if [ "X$1" = "X" ]; then
            $COMPOSE up -d
        else
            $COMPOSE up "$@"
        fi
        ;;
    backup)
        docker exec -i $NAME /usr/bin/arangodump --server.database "wokshop-food-db" --server.password "$DB_ROOT_PASSWORD" --overwrite true "$@"
        ;;
    restore)
        docker exec -i $NAME /usr/bin/arangorestore --server.database "wokshop-food-db" --server.password "$DB_ROOT_PASSWORD" "$@"
        ;;
    import)
        docker exec -i $NAME /usr/bin/arangoimport --server.database "wokshop-food-db" --server.password "$DB_ROOT_PASSWORD" "$@"
        ;;
    bash)
        docker exec -ti $NAME /bin/bash
        ;;
    dbsh)
        docker exec -ti $NAME /usr/bin/arangosh --server.password "$DB_ROOT_PASSWORD" "$@"
        ;;
    *)
        echo "usage: $0 start | stop | clean | build | logs | ps | backup | restore | import | bash | dbsh"
        ;;
esac
