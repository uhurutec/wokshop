# Wo(r)kshop

Dieses Projekt ist ein Workshop für eine Webanwendung,
die aus Micro-Frontends und Microservices besteht.
Dabei wird ein Wokshop erstellt, der in Kubernetes läuft.

**Hinweis: Wenn man den Wo(r)kshop von Beginn an durcharbeiten möchte,
kann das mehrere Stunden dauern.**

Das Projekt besteht aus drei Teilen bzw. Verzeichnissen,
um von unterschiedlichen Fortschritten starten zu können.

- **start/** : Der Wokshop wird von Grund auf erstellt.
  Es stehen nur grundlegende Dateien/Projekte zur Verfügung.
- **k8s/** : Der Wokshop wird in Kubernetes zum Laufen gebracht.
  Der Wokshop ist so weit implementiert, dass nur das Deployment auf Kubernetes umgesetzt werden muss.
  In diesem Fall kann man direkt zum [Kubernetes Deployment](#kubernetes-deployment) springen.
- **end/** : Der Wokshop ist im Rahmen dieses Projektes vollständig.
  In diesem Fall muss man aber trotzdem noch die Container Images bauen/herunterladen und Kubernetes (minikube) aufsetzen.

Der Wokshop kann am Ende wie folgt aussehen:

![Wo(r)kshop Ergebnis](wokshopWebpageFood.png)

Für den Wokshop wird Folgendes verwendet:

- Frontend: Angular, single-spa, PrimeNG
- Backend: Java, Spring Boot, gradle
- Datenbank: ArangoDB, docker-compose

Für das Kubernetes Deployment wird Folgendes verwendet:

- minikube
- docker
- docker-compose (Datenbank)

Dabei ist dieser Wokshop mit den folgenden Versionen entstanden.
(bei anderen Versionen können Schritte ggf. abweichen)

- (OS: linux x64)
- Angular CLI: 12.2.11
- Node: 14.19.0
- Package Manager: npm 6.14.16
- OpenJDK: 11.0.15
- Spring Boot: 2.7.1
- ArangoDB: 3.9
- PrimeNG: 13.4.1
- minikube version: v1.25.1
- docker-compose: 1.26.1

## Aufbau

Der Wokshop ist so aufgebaut,
dass er für die lokale Entwicklung folgendermaßen aussehen soll:

![Wo(r)kshop Local Development](workshopLocal.png)

Für die lokale Entwicklung werden die einzelnen Komponenten des Wokshops
wie folgt gestartet:

| Projekt       | Kommando                      |
|:--------------|:------------------------------|
| ui/root       | npm run start                 |
| ui/styleguide | npm run start                 |
| ui/nav        | npm run serve:single-spa:nav  |
| ui/food       | npm run serve:single-spa:food |
| service/food  | ./gradlew bootRun             |
| db/food       | ./wokshop-food-db.sh start    |

**Man beginnt im Ordner `start/`.**

## Projekte erstellen

Zuerst müssen die notwendigen Projekte erstellt werden.

### Micro-Frontends

Für das `root`- und `styleguide`-Micro-Frontend steht bereits ein Projekt zur Verfügung.
Hier muss man später die `root/src/index.ejs` anpassen.

Das `nav`- und `food`-Micro-Frontend muss man wie bei
[Single-Spa Angular Installation](https://single-spa.js.org/docs/ecosystem-angular#installation)
beschrieben erstellen.

Dazu muss man in das `ui`-Verzeichnis.

| Frontend | Style | Port |
|:---------|:------|:-----|
| nav      | CSS   | 4201 |
| food     | CSS   | 4202 |

Hier am Beispiel von `nav`:

```shell
~/wokshop/start/ui$ ng new nav --routing --prefix nav
? Which stylesheet format would you like to use? CSS
CREATE nav/README.md (1050 bytes)
CREATE nav/.editorconfig (274 bytes)
CREATE nav/.gitignore (604 bytes)
CREATE nav/angular.json (3021 bytes)
CREATE nav/package.json (1067 bytes)
CREATE nav/tsconfig.json (783 bytes)
CREATE nav/.browserslistrc (703 bytes)
CREATE nav/karma.conf.js (1420 bytes)
CREATE nav/tsconfig.app.json (287 bytes)
CREATE nav/tsconfig.spec.json (333 bytes)
CREATE nav/src/favicon.ico (948 bytes)
CREATE nav/src/index.html (289 bytes)
CREATE nav/src/main.ts (372 bytes)
CREATE nav/src/polyfills.ts (2820 bytes)
CREATE nav/src/styles.css (80 bytes)
CREATE nav/src/test.ts (788 bytes)
CREATE nav/src/assets/.gitkeep (0 bytes)
CREATE nav/src/environments/environment.prod.ts (51 bytes)
CREATE nav/src/environments/environment.ts (658 bytes)
CREATE nav/src/app/app-routing.module.ts (245 bytes)
CREATE nav/src/app/app.module.ts (393 bytes)
CREATE nav/src/app/app.component.css (0 bytes)
CREATE nav/src/app/app.component.html (24617 bytes)
CREATE nav/src/app/app.component.spec.ts (1064 bytes)
CREATE nav/src/app/app.component.ts (207 bytes)
✔ Packages installed successfully.
    Directory is already under version control. Skipping initialization of git.
```

Danach im Projekt `single-spa-angular` hinzufügen.

```shell
~/wokshop/start/ui$ cd nav/
~/wokshop/start/ui/nav$ ng add single-spa-angular
ℹ Using package manager: npm
✔ Found compatible package version: single-spa-angular@5.0.2.
✔ Package information loaded.
 
The package single-spa-angular@5.0.2 will be installed and executed.
Would you like to proceed? Yes
✔ Package successfully installed.
? Does your application use Angular routing? Yes
? What port should your project run on? 4201
    Added 'single-spa' as a dependency
    Added 'single-spa-angular' as a dependency
    Added '@angular-builders/custom-webpack' as a dependency
    Generated 'main.single-spa.ts
    Generated 'single-spa-props.ts
    Generated asset-url.ts
    Generated extra-webpack.config.js
    Using @angular-builders/custom-webpack builder.
    Updated angular.json configuration
    @angular-builders/custom-webpack:browser
    Warning: Since routing is enabled, an additional manual
    configuration will be required, see https://single-spa.js.org/docs/ecosystem-angular/#configure-routes
CREATE extra-webpack.config.js (303 bytes)
CREATE src/main.single-spa.ts (932 bytes)
CREATE src/app/empty-route/empty-route.component.ts (143 bytes)
CREATE src/single-spa/asset-url.ts (502 bytes)
CREATE src/single-spa/single-spa-props.ts (333 bytes)
UPDATE package.json (1335 bytes)
UPDATE tsconfig.app.json (196 bytes)
UPDATE angular.json (3303 bytes)
```

Danach muss die Datei `src/app/app-routing.module.ts` in beiden Projekten erweitert werden,
sodass sie wie folgt aussieht:

```typescript
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {APP_BASE_HREF} from '@angular/common';
import {EmptyRouteComponent} from "./empty-route/empty-route.component";

const routes: Routes = [
  { path: '**', component: EmptyRouteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],

})
export class AppRoutingModule { }
```

#### PrimeNG hinzufügen

Für den Wokshop wird [PrimeNG](https://www.primefaces.org/primeng-v13/)
zu den Micro-Frontends `nav` und `food` hinzugefügt.

Dazu muss folgendes im jeweiligen Projekt ausgeführt werden.

```shell
npm install primeng --save
npm install primeicons --save
npm install primeflex --save
```

Danach muss man noch die Styles in der `angular.json` unter `styles` hinzufügen:

```text
            "styles": [
              "src/styles.css",
              "node_modules/primeicons/primeicons.css",
              "node_modules/primeng/resources/themes/md-light-deeppurple/theme.css",
              "node_modules/primeng/resources/primeng.min.css",
              "node_modules/primeflex/primeflex.scss"
            ],
```

#### assets konfigurieren

Damit Assets im Projekt gefunden werden können, muss man eine Pipe erstellen,
welche die notwendige URL zu Bildern hinzufügt (z.B. `http://localhost:4202/`).

```shell
ng g pipe asset-url
```

Die Pipeline muss für single-spa wie folgt angepasst werden:

```text
import { assetUrl } from "../single-spa/asset-url";

...

    transform(value: string): string {
      return assetUrl(value);
    }
```

### Microservice

Der `food`-Microservice wird mit dem [Spring Initializr](https://start.spring.io/) erstellt.
Daraus kommt eine `.zip`-Datei, aus der der Service entpackt wird.

![Spring Initializr Konfiguration](spring-initializr-service.png)


## Micro-Frontends implementieren

### root

Für single-spa muss die `index.ejs` von `root` erweitert werden.

Es wird eine Importmap benötigt, welche man wie folgt hinzufügt:

```text
    <!-- Add importmap -->
    <meta name="importmap-type" content="systemjs-importmap">
    <script type="systemjs-importmap" src="config/importmap.json">
    </script>
    <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/single-spa/5.5.0/system/single-spa.min.js"
          as="script" crossorigin="anonymous"/>
    <script src="https://unpkg.com/import-map-overrides@1.15.1/dist/import-map-overrides.js"></script>
```

In diesem Fall wird die Datei `config/importmap.json` verwiesen.
Für die lokale Entwicklung befindet sich die Datei unter `public/importmap.json`,
beim Kubernetes Deployment wird sie über eine ConfigMap eingebunden.

Als Nächstes wird das Routing hinzugefügt.
Dabei ist das `nav`-Micro-Frontend permanent zu sehen (Route `/`),
das `food`-Micro-Frontend jedoch nur unter `/food`.

```text
    <template id="single-spa-layout">
        <single-spa-router>
            <route path="/">
                <nav class="topnav">
                    <application name="@wokshop/nav"></application>
                </nav>
            </route>
            <div class="main-content">
                <route path="food">
                    <application name="@wokshop/food"></application>
                </route>
            </div>
        </single-spa-router>
    </template>
```

Die `workshop-*-config.js` müssen ebenfalls importiert werden:

```text
<script>
    <!-- Import configs of root and styleguide -->
    System.import('@wokshop/styleguide');
    System.import('@wokshop/root-config');
</script>
```

Als Letztes kann noch fürs Debugging `import-map-overrides` in der Datei hinzugefügt werden.
`import-map-overrides` wurde bereits vorher importiert.

```text
<!-- Add import-map-overrides for debugging -->
<import-map-overrides-full></import-map-overrides-full>
```

### nav

Wir benutzen das [TabMenu von PrimeNG](https://www.primefaces.org/primeng-v13/tabmenu) für die Menüleiste.
Dazu müssen die folgenden Dateien angepasst werden:

- app.module.ts:
- app.component.ts:
- app.component.html:

_Hinweis: Wer nicht weiter weiß
oder sich nicht mit "Pixel schieben" beschäftigen möchte,
der kann auch gerne hier und auch in weiteren Schritten im Verzeichnis `end/` nachschauen. :)_

### food

Wir verwenden das [TabView von PrimeNG](https://www.primefaces.org/primeng-v13/tabview) für die Tabs der Speisen.

Die Daten der Speisen sehen folgendermaßen aus:

| Name        | Typ    | Beispiel             |
|:------------|:-------|:---------------------|
| id          | string | "dessert/208"        |
| key         | string | "208"                |
| name        | string | "Lorem Ipsum"        |
| description | string | "Product Description |
| image       | string | "P8070150.JPG"       |
| price       | number | 7.5                  |
| category    | string | "Hot"                |

Dazu müssen die folgenden Dateien angepasst oder erstellt werden:

- app.module.ts
- food.ts:
- app.component.html:

Die Bilder sind zum Kopieren im `images/`-Verzeichnis zu finden.

Für die Liste der Speisen wird [DataView von PrimeNG](https://www.primefaces.org/primeng-v13/dataview) verwendet.
Dazu muss eine neue Komponente erstellt und angepasst werden.

```shell
ng g component food-panel
```

Für das `food`-Micro-Frontend muss `@angular/cdk` installiert sein,
falls nicht bereits geschehen.

```shell
npm install @angular/cdk@12.2.11
```

## Microservice implementieren

Für den `food`-Microservice wird das [ArangoDB Spring Framework](https://www.arangodb.com/docs/stable/drivers/spring-data-getting-started.html) verwendet,
um mit der `food`-ArangoDB zu kommunizieren.

Das Framework muss wie folgt in der `build.gradle` hinzugefügt
und installiert werden.

```text
	implementation 'com.arangodb:arangodb-spring-data:3.6.0'
```

Anschließend muss die folgende Annotation bei der `FoodApplication` hinzugefügt werden.

```text
@EnableArangoRepositories
```

Der Service wird folgendermaßen strukturiert:

| Verzeichnis    | Beschreibung                           |
|:---------------|:---------------------------------------|
| config/        | Für die Konfiguration(en) des Wokshops |
| controller/    | Für die REST-API-Implementierung       |
| database/      | Für die Kommunikation mit der ArangoDB |
| database/model | Für die Datenmodelle der ArangoDB      |

Im `resources`-Verzeichnis muss außerdem die Konfigurationsdatei `application.yml`
erstellt werden, in der die Konfiguration zur Datenbank definiert (und ausgelesen) wird.
Beim Kubernetes Deployment wird sie dann über eine ConfigMap integriert.

Für den `food`-Microservice müssen die folgenden Dateien erstellt werden:

- config/Config.java
- config/ArangoDbConfig.java
- resources/application.yml
- controller/FoodController.java
- database/AppetizerRepository.java
- database/MaincourseRepository.java
- database/DessertRepository.java
- database/model/Appetizer.java
- database/model/Maincourse.java
- database/model/Dessert.java

Für den Wokshop genügt es, die Liste der jeweiligen Speisen abfragen zu können.

```shell
$ curl localhost:8080/food/dessert | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   733    0   733    0     0   238k      0 --:--:-- --:--:-- --:--:--  238k
[
  {
    "key": "204",
    "id": "dessert/204",
    "name": "Nonumy Eirmod",
    "description": "Product Description",
    "image": "P1010136.JPG",
    "price": 8,
    "category": "Cold"
  },
  {
    "key": "205",
    "id": "dessert/205",
    "name": "Elitr Sed Diam",
    "description": "Product Description",
    "image": "P2200021.JPG",
    "price": 9,
    "category": "Cold"
  },
  {
    "key": "206",
    "id": "dessert/206",
    "name": "Consetetur Sadipscing",
    "description": "Product Description",
    "image": "P3314911.JPG",
    "price": 8.5,
    "category": "Cold"
  },
  {
    "key": "207",
    "id": "dessert/207",
    "name": "Dolor Sit Amet",
    "description": "Product Description",
    "image": "P8010134.JPG",
    "price": 10,
    "category": "Hot"
  },
  {
    "key": "208",
    "id": "dessert/208",
    "name": "Lorem Ipsum",
    "description": "Product Description",
    "image": "P8070150.JPG",
    "price": 7.5,
    "category": "Hot"
  }
]
```

## Kubernetes Deployment

Für das Kubernetes Deployment müssen zunächst die Container Images gebaut
und anschließend das Kubernetes Deployment in `YAML`-Dateien definiert werden.

Die lokale Entwicklung sieht im Kubernetes Deployment wie folgt aus:

![Wo(r)kshop Kubernetes Deployment](workshopKubernetes.png)

Der Wokshop besteht am Ende aus den folgenden Teilen in Kubernetes:

```text
$ kubectl get all -n wokshop
NAME                                        READY   STATUS    RESTARTS   AGE
pod/wokshop-db-food-0                       1/1     Running   0          88m
pod/wokshop-service-food-76747b8fbf-sxbp4   1/1     Running   0          88m
pod/wokshop-ui-food-694d96f88b-76w8x        1/1     Running   0          88m
pod/wokshop-ui-food-694d96f88b-xwr62        1/1     Running   0          88m
pod/wokshop-ui-nav-58fcb448c5-c5kr7         1/1     Running   0          88m
pod/wokshop-ui-nav-58fcb448c5-n6vz6         1/1     Running   0          88m
pod/wokshop-ui-root-57cdc8d7bc-gqsmt        1/1     Running   0          88m
pod/wokshop-ui-styleguide-fdf7654cc-mrhs2   1/1     Running   0          88m

NAME                            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service/food                    ClusterIP   10.102.34.66     <none>        8529/TCP         88m
service/wokshop-service-food    NodePort    10.99.59.205     <none>        8080:31509/TCP   88m
service/wokshop-ui-food         NodePort    10.108.114.36    <none>        80:31680/TCP     88m
service/wokshop-ui-nav          NodePort    10.111.39.197    <none>        80:31892/TCP     88m
service/wokshop-ui-root         NodePort    10.104.216.233   <none>        80:30395/TCP     88m
service/wokshop-ui-styleguide   NodePort    10.110.7.102     <none>        80:32008/TCP     88m

NAME                                    READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/wokshop-service-food    1/1     1            1           88m
deployment.apps/wokshop-ui-food         2/2     2            2           88m
deployment.apps/wokshop-ui-nav          2/2     2            2           88m
deployment.apps/wokshop-ui-root         1/1     1            1           88m
deployment.apps/wokshop-ui-styleguide   1/1     1            1           88m

NAME                                              DESIRED   CURRENT   READY   AGE
replicaset.apps/wokshop-service-food-76747b8fbf   1         1         1       88m
replicaset.apps/wokshop-ui-food-694d96f88b        2         2         2       88m
replicaset.apps/wokshop-ui-nav-58fcb448c5         2         2         2       88m
replicaset.apps/wokshop-ui-root-57cdc8d7bc        1         1         1       88m
replicaset.apps/wokshop-ui-styleguide-fdf7654cc   1         1         1       88m

NAME                               READY   AGE
statefulset.apps/wokshop-db-food   1/1     88m
```

### Container Images bauen

In diesem Abschnitt werden die einzelnen Container Images gebaut.

Möchte man den Schritt überspringen, kann man auch die Container Images aus der
[Container Registry](https://gitlab.com/uhurutec/wokshop/container_registry) verwenden.
Dann muss man aber auch die Namen der Container Images in den weiteren Schritten entsprechend anpassen
oder die Images aus der Registry lokal entsprechend taggen.

#### Micro-Frontends

Das `root`- und `styleguide`-Micro-Frontend benötigen das gleiche Dockerfile.

```text
### STAGE 1: Build ###
FROM node:16-alpine AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json /usr/src/app/
RUN npm ci
COPY . /usr/src/app
RUN npm run build

### STAGE 2: Run ###
FROM nginx:1.20.1-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
```

Das Container Image kann dann beispielsweise wie folgt mit docker gebaut werden.

```shell
docker build -t wokshop/ui/root:0.0.0 .
docker build -t wokshop/ui/styleguide:0.0.0 .
```

Für `ui/nav` und `ui/food` müssen noch Anpassungen gemacht werden.
Hier am Beispiel von `ui/nav`.

- angular.json anpassen:

```text
      "architect": {
        "build": {
          ...
          "options": {
            ...
            "deployUrl": "//micro.wokshop.uhurutec.com/nav/"
          },
          "configurations": {
            "production": {
              "budgets": [
                {
                  "type": "initial",
                  "maximumWarning": "2mb",
                  "maximumError": "4mb"
                },
```

- nginx.conf erstellen:

```text
events{}

http {

    include /etc/nginx/mime.types;

    server {
        listen 80;
        server_name localhost;
        root /usr/share/nginx/html/nav;

        location / {
            try_files $uri $uri/ /index.html;
            add_header "Access-Control-Allow-Origin" "*";
            add_header  "Access-Control-Allow-Methods" "GET, POST, PUT, DELETE, PATCH, OPTIONS";
            add_header  "Access-Control-Allow-Headers" "X-Requested-With, content-type, Authorization";
        }
    }
}
```

Das Dockerfile muss ebenfalls erstellt werden (dabei ähneln sich die beiden Micro-Frontends).

```text
### STAGE 1: Build ###
FROM node:16-alpine AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json /usr/src/app/
RUN npm ci
COPY . /usr/src/app
RUN npm run build:single-spa:nav

### STAGE 2: Run ###
FROM nginx:1.20.1-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
```

Anschließend kann das Container Image für das Micro-Frontend gebaut werden:

```shell
docker build -t wokshop/ui/nav:0.0.0 .
```

#### Microservices

Beim Wokshop gibt es nur den `food`-Service.

Der Service wird zur Einfachheit zuerst manuell gebaut
und dann anschließend in das Container Image kopiert.

Dabei wird die Konfiguration im Dockerfile angepasst.

```text
FROM tomcat:9.0.60-jdk11-openjdk-slim-buster

ADD build/libs/food-*.war /usr/local/tomcat/webapps/ROOT.war

# Application config
RUN mkdir /app_conf/

# Create "$CATALINA_HOME/bin/setenv.sh"
# Escape char : https://docs.docker.com/engine/reference/builder/#/escape
RUN echo "# Add app_conf to Tomcat CLASSPATH" > $CATALINA_HOME/bin/setenv.sh
RUN echo "CLASSPATH=/app_conf/" >> $CATALINA_HOME/bin/setenv.sh
RUN chmod 750 $CATALINA_HOME/bin/setenv.sh

EXPOSE 8080
CMD ["catalina.sh", "run"]
```

**Wichtig: Die Datei `src/main/resources/application.yml` muss vorher entfernt/umbenannt werden,
da sonst die ConfigMap beim Kubernetes Deployment Probleme hat, die Datei zu überschreiben.**

```shell
./gradlew clean bootWar
docker build -t wokshop/service/food:0.0.0 .
```

#### Datenbank

Bei der Datenbank wird das Container Image mit `docker-compose`
mithilfe des Skripts `wokshop-food-db.sh` gebaut.

```shell
./wokshop-food-db.sh build
docker tag wokshop/db/food:latest wokshop/db/food:0.0.0
```

### Kubernetes Deployment Projekt

Der Wokshop wird im Kubernetes Namespace `wokshop` erstellt.

Das Kubernetes Deployment wird in die folgenden Verzeichnisse unterteilt
und in den weiteren Abschnitten beleuchtet.

- prerequisites/
- db/
- service/
- ui/

#### prerequisites/

Für die Datenbank benötigt es ein Persistent Volume (PV),
damit die Daten über die Lebensdauer des Containers hinweg erhalten bleiben.

Für den Wokshop wird lokaler Speicher verwendet.
In der produktiven Umgebung kann dies zum Beispiel durch Ceph Storage abgelöst werden.

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wokshop

---

apiVersion: v1
kind: PersistentVolume
metadata:
  name: "pv-shared-food-db"
  namespace: wokshop
spec:
  storageClassName: shared-food-db
  capacity:
    storage: "2Gi"
  accessModes:
    - "ReadWriteOnce"
  hostPath:
    path: /shared/wokshop-food-db
```

#### db/

Für die ArangoDB wird ein StatefulSet angelegt.
Die Kommunikation zwischen der Datenbank und dem Service läuft innerhalb des Clusters,
weshalb die Datenbank einen ClusterIP-Service erhält.

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wokshop

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: wokshop-db-food
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-db-food
    app.kubernetes.io/instance: wokshop-db-food
    app.kubernetes.io/version: 0.0.2
    app.kubernetes.io/component: database
    app.kubernetes.io/part-of: wokshop-ui
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: UhuruTec
spec:
  replicas: 1
  serviceName: wokshop-db-food
  selector:
    matchLabels:
      app.kubernetes.io/name: wokshop-db-food
  template:
    metadata:
      labels:
        app.kubernetes.io/name: wokshop-db-food
    spec:
      containers:
        - name: wokshop-db-food
          image: wokshop/db/food:0.0.0
          imagePullPolicy: Never
          env:
            - name: ARANGO_ROOT_PASSWORD
              value: "wokshop"
          volumeMounts:
            - name: data
              mountPath: /var/lib/arangodb3

  # the following volumeClaimTemplates will change in "real" kubernetes environment
  # depending on how shared volumes are implemented there
  #
  volumeClaimTemplates:
    - metadata:
        name: data
      spec:
        storageClassName: shared-food-db
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 1Gi

---

apiVersion: v1
kind: Service
metadata:
  name: food
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-db-food
    app.kubernetes.io/version: 0.0.0
spec:
  type: ClusterIP
  ports:
    - name: food
      port: 8529
  selector:
    app.kubernetes.io/name: wokshop-db-food
```

#### service/

Der `food`-Microservice benötigt die folgenden Komponenten:

- ConfigMap
- Deployment
- Service
- Ingress

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wokshop

---

apiVersion: v1
kind: ConfigMap
metadata:
  name: config-wokshop-service-food
  namespace: wokshop
data:
  application.yml: |
    config:
      databaseConfig:
        server: food.wokshop.svc.cluster.local
        port: 8529
        username: cloudland
        password: wokshop

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: wokshop-service-food
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-service-food
    app.kubernetes.io/instance: wokshop-service-food
    app.kubernetes.io/version: 0.0.0
    app.kubernetes.io/component: wokshop-service-food
    app.kubernetes.io/part-of: backend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: UhuruTec
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: wokshop-service-food
  template:
    metadata:
      labels:
        app.kubernetes.io/name: wokshop-service-food
    spec:
      containers:
        - name: wokshop-service-food
          image: wokshop/service/food:0.0.0
          imagePullPolicy: Never
          volumeMounts:
            - name: config-wokshop-service-food
              mountPath: /app_conf
      volumes:
        - name: config-wokshop-service-food
          configMap:
            name: config-wokshop-service-food

---

apiVersion: v1
kind: Service
metadata:
  name: wokshop-service-food
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-service-food
    app.kubernetes.io/instance: wokshop-service-food-load-balancer
    app.kubernetes.io/version: 0.0.0
    app.kubernetes.io/component: wokshop-service-food
    app.kubernetes.io/part-of: backend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: UhuruTec
spec:
  type: NodePort
  ports:
    - name: wokshop-service-food
      port: 8080
  selector:
    app.kubernetes.io/name: wokshop-service-food

---

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wokshop-service-food-ingress
  namespace: wokshop
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  ingressClassName: nginx
  rules:
    - host: service.wokshop.uhurutec.com
      http:
        paths:
          - path: /food/(.+)
            pathType: Prefix
            backend:
              service:
                name: wokshop-service-food
                port:
                  number: 8080
```

#### ui/

Das `root`-Micro-Frontend benötigt die folgenden Komponenten:

- ConfigMap
- Deployment
- Service
- Ingress

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wokshop

---

apiVersion: v1
kind: ConfigMap
metadata:
  name: config-wokshop-ui-root
  namespace: wokshop
data:
  config.json: |
    {
      "foodServiceUrl": "http://service.wokshop.uhurutec.com/food"
    }
  importmap.json: |
    {
      "imports": {
        "@wokshop/root-config": "//wokshop.uhurutec.com/wokshop-root-config.js",
        "@wokshop/styleguide": "//micro.wokshop.uhurutec.com/styleguide/wokshop-styleguide.js",
        "@wokshop/nav": "//micro.wokshop.uhurutec.com/nav/main.js",
        "@wokshop/food": "//micro.wokshop.uhurutec.com/food/main.js",
        "single-spa": "https://cdnjs.cloudflare.com/ajax/libs/single-spa/5.5.0/system/single-spa.min.js"
      }
    }

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: wokshop-ui-root
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-ui-root
    app.kubernetes.io/instance: wokshop-ui-root
    app.kubernetes.io/version: 0.0.1
    app.kubernetes.io/component: wokshop-ui-root
    app.kubernetes.io/part-of: frontend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: uview
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: wokshop-ui-root
  template:
    metadata:
      labels:
        app.kubernetes.io/name: wokshop-ui-root
    spec:
      containers:
        - name: wokshop-ui-root
          image: wokshop/ui/root:0.0.0
          imagePullPolicy: Never
          volumeMounts:
            - name: config-wokshop-ui-root
              mountPath:  /usr/share/nginx/html/config
      volumes:
        - name: config-wokshop-ui-root
          configMap:
            name: config-wokshop-ui-root

---

apiVersion: v1
kind: Service
metadata:
  name: wokshop-ui-root
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-ui-root
    app.kubernetes.io/instance: wokshop-ui-root-load-balancer
    app.kubernetes.io/version: 0.0.1
    app.kubernetes.io/component: wokshop-ui-root
    app.kubernetes.io/part-of: frontend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: uview
spec:
  type: NodePort
  ports:
    - name: wokshop-ui-root
      port: 80
  selector:
    app.kubernetes.io/name: wokshop-ui-root

---

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wokshop-ui-root-ingress
  namespace: wokshop
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  ingressClassName: nginx
  rules:
    - host: wokshop.uhurutec.com
      http:
        paths:
          - path: /(.*)
            pathType: Prefix
            backend:
              service:
                name: wokshop-ui-root
                port:
                  number: 80
```

Die Kubernetes Deployments für die restlichen Micro-Frontends ähneln sich.
Hier am Beispiel vom `styleguide`-Micro-Frontend.

Es wird benötigt:

- Deployment
- Service
- Ingress

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: wokshop

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: wokshop-ui-styleguide
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-ui-styleguide
    app.kubernetes.io/instance: wokshop-ui-styleguide
    app.kubernetes.io/version: 0.0.1
    app.kubernetes.io/component: wokshop-ui-styleguide
    app.kubernetes.io/part-of: frontend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: UhuruTec
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: wokshop-ui-styleguide
  template:
    metadata:
      labels:
        app.kubernetes.io/name: wokshop-ui-styleguide
    spec:
      containers:
        - name: wokshop-ui-styleguide
          image: wokshop/ui/styleguide:0.0.0
          imagePullPolicy: Never

---

apiVersion: v1
kind: Service
metadata:
  name: wokshop-ui-styleguide
  namespace: wokshop
  labels:
    app.kubernetes.io/name: wokshop-ui-styleguide
    app.kubernetes.io/instance: wokshop-ui-styleguide-load-balancer
    app.kubernetes.io/version: 0.0.1
    app.kubernetes.io/component: wokshop-ui-styleguide
    app.kubernetes.io/part-of: frontend
    app.kubernetes.io/managed-by: kubectl
    app.kubernetes.io/created-by: UhuruTec
spec:
  type: NodePort
  ports:
    - name: wokshop-ui-styleguide
      port: 80
  selector:
    app.kubernetes.io/name: wokshop-ui-styleguide

---

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: wokshop-ui-styleguide-ingress
  namespace: wokshop
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  ingressClassName: nginx
  rules:
    - host: micro.wokshop.uhurutec.com
      http:
        paths:
          - path: /styleguide/(.+)
            pathType: Prefix
            backend:
              service:
                name: wokshop-ui-styleguide
                port:
                  number: 80
```

### Minikube

Der Wokshop kann nun in minikube aufgesetzt werden.

Es empfiehlt sich einen Alias zu setzen.
Der Abschnitt verwendet den folgenden Alias:

```shell
alias kubectl='minikube kubectl --'
```

Mit dem folgenden Befehl wird minikube gestartet.
Dabei das CNI `calico` verwendet und das Verzeichnis `~/minikube/shared`
für die ArangoDB gemountet.
(Ggf. muss das Verzeichnis vorher angelegt werden)

Anschließend wird Ingress aktiviert.

```shell
minikube start --cni=calico --mount --mount-string="${HOME}/minikube/shared:/shared"
minikube addons enable ingress
```

Leider hat sich bei uns ein Problem ergeben, wenn wir Ingress Resourcen erstellt haben:
Die Adressen wurden alle auf localhost gebunden und waren deswegen nicht erreichbar.

Um dieses Problem zu lösen, muss Folgendes gemacht werden:

```shell
kubectl edit deployments.apps -n ingress-nginx ingress-nginx-controller
```

Hier muss folgende Zeile gelöscht werden:

```text
      - --publish-status-address=localhost
```

Ab hier sollte minikube mit allen Voraussetzungen laufen.

Es werden mindestens zwei minikube-Knoten hinzugefügt:

```shell
minikube node add
minikube node add
```

In der `/etc/hosts` müssen außerdem folgende Einträge hinzugefügt werden,
damit der Wokshop aufgelöst werden kann.

Die IP-Adresse kann ggf. abweichen.
In dem Fall kann man mit `minikube node list` nachschauen.

```text
# For wokshop running in minikube
192.168.49.2 wokshop.uhurutec.com
192.168.49.2 micro.wokshop.uhurutec.com
192.168.49.2 service.wokshop.uhurutec.com
```

Als Nächstes werden alle Container Images aus den Deployments in minikube geladen:

```shell
find db/ service/ ui/ -name '*.yaml' | xargs grep image: | sed 's/^.*image://' | xargs minikube image load
```

Das sollte zu folgendem Ergebnis führen.
Es kann aber auch sein, dass z.B. ein Container Image "verschollen" geht.
In dem Fall muss man das Container Image einfach nochmal laden.

```text
$ minikube image list | grep wokshop
docker.io/wokshop/ui/styleguide:0.0.0
docker.io/wokshop/ui/root:0.0.0
docker.io/wokshop/ui/nav:0.0.0
docker.io/wokshop/ui/food:0.0.0
docker.io/wokshop/service/food:0.0.0
docker.io/wokshop/db/food:0.0.0
```

Anschließend können die `YAML`-Dateien angewendet werden.
Hier ist es wichtig, zuerst das PV anzulegen.

```shell
kubectl apply -f prerequisites/
kubectl apply -f db/
kubectl apply -f service/
kubectl apply -f ui/
```

Nun kann der Wokshop über `http://wokshop.uhurutec.com` erreicht werden.